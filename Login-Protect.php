<?php	if ( ! defined( 'GO_HOME' ) ) define( 'GO_HOME', true );
//
//	Module to protect wp-login from some brute-forcing attacks. - EXPERIMENTAL.
//
//	Soon to become part of my new "Dragon-Armor Web-Fortress" project!
//
//	By: 3(L!P$3 [)R@90[\] , Of: Eclipse Web-Designs,
//	A FRESH WEB SOLUTION. ( https://afreshwebsolution.com )
//	Version: 1.03.02,	Released: 2022-03-20
//
//	To Use: ==> In .htaccess file: --> RewriteRule (^|/)wp-login\.php$ /<this file's name>.php [NS,L,QSA]
//


//	User-settable values.
define( 'NO_DEFAULT_COOKIE', false );	//	Using a different test-cookie name?
define( 'LOGIN_MODULE', "wp-login.php" ); //	Default value normally would be: 'wp-login.php'
define( 'USE_EMAIL_NAME', true );	//	Force logins by using email, instead of just "username"?
define( 'GZIP_COOKIE', true );	//	If GZIP capabilities are available, use them to store the "cookie".
define( 'HANGOUT', 180 );	//	Limit "loitering time" on Log in screen to max seconds.


//	Prevent mischievous nonsense!
foreach( [ 'HTTP_HOST', 'SERVER_NAME', 'DOCUMENT_ROOT', 'HTTPS', 'SERVER_ADDR', 'REMOTE_ADDR', 'REQUEST_METHOD', 'PHP_SELF', 'HTTP_USER_AGENT', 'HTTP_REFERER' ] as $key ) {
	if ( isset( $_SERVER[$key] ) && $_SERVER[$key] != htmlentities( $_SERVER[$key] ) ) {
		header( 'HTTP/1.1 400 Bad Request', true, 400 );
		die( 'Bad Request Header(s)!' );
		}
	}

$_SERVER['REMOTE_ADDR'] = htmlentities( $_SERVER['REMOTE_ADDR'], ENT_QUOTES, 'UTF-8' );

//	Compatibility shim.
if ( isset( $_POST['user_login'] ) ) $_POST['log'] = $_POST['user_login'];

//	Some common bad log-in user names! (May eventually use a "dictionary"-file for this.)
if ( false === ( $bad_logins = @file( dirname( $_SERVER['DOCUMENT_ROOT'] ) . '/bad_logins.txt.cfg' ) ) ) {
	$bad_logins = [
		'admin',
		'ADMIN',
		'user',
		'USER',
		'admin123',
		'ADMIN123',
		'login',
		'LOGIN',
		'administrator',
		'ADMINISTRATOR',
		'administrador',
		'ADMINISTRADOR',
		'superuser',
		'SUPERUSER',
		];
	}

//	Clean up 'Bad-Logins' array.
for ( $l = 0; $l < count( $bad_logins ); $l++ ) $bad_logins[$l] = trim( $bad_logins[$l] );

//	Forced exit for BAD BEHAVIORS.
function LoPro_drop_dead_mate() {
	header( 'HTTP/1.1 400 BAD REQUEST', true, 400 );
	die( 'Drop dead, mate!' );
}


//	MUST access from the main page link, NOT directly!
if ( ( @empty( $_SERVER['HTTP_HOST'] ) || @$_SERVER['HTTP_HOST'] == '-' ) ||
	( @empty( $_SERVER['HTTP_REFERER'] ) || @strpos( $_SERVER['HTTP_REFERER'], $_SERVER['SERVER_NAME'] ) === false ) ||
//	And, NO empty or faked UA-strings, either!
	( @empty( $_SERVER['HTTP_USER_AGENT'] ) || @$_SERVER['HTTP_USER_AGENT'] == '-' ) ) {
		LoPro_drop_dead_mate();
	}

//	Say NO, to default "test-cookie" usage! ( Actual Test Cookie field name is set in wp-config.php )
if ( isset( $_COOKIE['wordpress_test_cookie'] ) && $_COOKIE['wordpress_test_cookie'] == 'WP Cookie check' /* && $_SERVER['REQUEST_METHOD'] == 'POST' */ &&
	defined( 'NO_DEFAULT_COOKIE' ) && NO_DEFAULT_COOKIE ) {
		LoPro_drop_dead_mate();
	}

//	If not using "HTTP/1.1" for "POST" methods, expect it to be from a spammer or renegade bot!
if ( $_SERVER['REQUEST_METHOD'] == 'POST' && $_SERVER['SERVER_PROTOCOL'] != 'HTTP/1.1' ) LoPro_drop_dead_mate();


function LoPro_get_file( $path_name ) {
	if ( function_exists( 'gzfile' ) && true === GZIP_COOKIE ) return @gzfile( $path_name );
	return @file( $path_name );
}

function LoPro_put_file( $path_name, $the_data ) {
	if ( function_exists( 'gzencode' ) && true === GZIP_COOKIE ) return @file_put_contents( $path_name, gzencode( $the_data, 9, FORCE_GZIP ), LOCK_EX );
	return @file_put_contents( $path_name, $the_data, LOCK_EX );
}

function LoPro_put2( $path_name, $the_data ) {
	if ( false !== LoPro_put_file( $path_name, $the_data ) ) return chmod( $path_name, 0640 );
	return false;
}

function LoPro_gen_session_name() {
	return str_replace( [ '/', '+', '=' ], [ '-', '!', '_' ], base64_encode( sha1( date( 'Ymd' ) . "-" . $_SERVER['REMOTE_ADDR'], true ) ) );
}

//	Custom COOKIE content encoder/decoder functions.

function LoPro_cookie_encode( $cookie_data ) {
	if ( function_exists( 'json_encode' ) ) return json_encode( $cookie_data, JSON_UNESCAPED_SLASHES );
	return serialize( $cookie_data );
}

function LoPro_cookie_decode( $cookie_data ) {
	if ( is_array( $cookie_data ) ) $cookie_data = implode( "", $cookie_data );
	if ( function_exists( 'json_decode' ) ) return @json_decode( $cookie_data, true );
	return @unserialize( $cookie_data );
}

//	Validate our cookies against a pre-calculated checksum. - Prevents "poisoned" cookies! - YUCK!!!
function LoPro_check_cookie( $some_data = [] ) {
	if ( empty( $some_data ) || ! is_array( $some_data ) ) return false;

	$checksum = '';

	foreach ( $some_data as $check_field => $this_data ) {
//		One BAD BASTARD of a checksum calculation! This checks the field-names as well as the data AND their positions.
		if ( $check_field != 'session-hash' ) $checksum .= sha1( "$check_field{$some_data[$check_field]}$checksum", true );
		}

//	Return checksum results of what we found.
	return base64_encode( sha1( gzdeflate( $checksum, 9 ), true ) );
}


//	Set special ( very ) temporary session value for our custom cookie.
$my_session = [ 'id' => LoPro_gen_session_name() ];

//	Our custom "session" cookie folder. ( We use our own cookies here. )
$cookie_path = rtrim( ini_get( 'session.save_path' ), '/' ) . '/wpbf_cookies/';
if ( ! file_exists( $cookie_path ) ) mkdir( $cookie_path, 0750 );

//	Set for Logout flag.
$logging_out = ( @$_GET['action'] == 'logout' && ! @empty( $_GET['_wpnonce'] ) ? true : false );

if ( file_exists( "$cookie_path.{$my_session['id']}" ) ) {
	if ( false !== ( $sess_data = LoPro_get_file( "$cookie_path.{$my_session['id']}" ) ) ) {
		$sess_data = LoPro_cookie_decode( implode( "", $sess_data ) );
		}

	else {
		die( 'Bad cookie file!' );
		}
	}

else $sess_data = '';

//	Check cookie validity to detect possible tampering.
if ( @is_array( $sess_data ) && ! $logging_out ) {
	if ( @$sess_data['id'] == $my_session['id']  && @$sess_data['session-hash'] == LoPro_check_cookie( $sess_data ) && count( $sess_data ) == 10 )
		$my_session = array_merge( $my_session, $sess_data );

	else {	//	Make "bad cookie" unusable without destroying it.
		if ( @$sess_data['visit-result'] != 'Bad Cookie!' ) {
			$sess_data['visit-result'] = $sess_data['session-hash'] = 'Bad Cookie!';
			$sess_data['this-session'] = '-';
			$sess_data['login-attempts'] = -1;

			LoPro_put2( "$cookie_path.{$my_session['id']}", LoPro_cookie_encode( $sess_data ) );
			}

		LoPro_drop_dead_mate();
		}
	}

header( 'Cache-Control: no-store,no-cache,must-revalidate,pre-check=0,post-check=0,max-age=0', true );
header( 'Pragma: no-cache', true );

//	Make sure to clean up old cookie when we log out.
if ( isset( $_GET['loggedout'] ) && 'true' == $_GET['loggedout'] ) {
	@unlink( "$cookie_path.{$my_session['id']}" );
	unset( $cookie_path, $my_session );

	if ( defined( 'GO_HOME' ) && GO_HOME === true ) {	//	This setup, was to work well with my Anti-Hammering software!

		$to_home = 'http' . ( @$_SERVER['HTTPS'] == 'on' ? 's': '' ) . '://' . $_SERVER['SERVER_NAME'] . dirname( $_SERVER['PHP_SELF'] );

		header( 'Content-Type: text/html; charset=UTF-8' );
		header( 'Refresh: 3; url=' . $to_home );

		?><!DOCTYPE html>
<html>
<head>
	<meta name='charset' content='UTF-8' />
	<meta name='robots' content='noindex,nofollow,nocache' />

	<title>Logout Successful!</title>
	<!--link rel='stylesheet' href='/styles/login.css' /-->
	<style>
/*		body {background:url('/images/tiles/NightSky2.gif');} */
		h1,h2,h3 {text-align:center;}
		h1 {color:medblue;}
/*		div {max-width:650px;min-width:220px;margin:10px auto;padding:5px;}	*/
/*		#secondary {background:url('/images/tiles/Fader_Horiz_1b.png') top right repeat-y;border-radius:10px;} */
	</style>
</head>

<body>
	<div id='login'>
		<h1><?php echo $_SERVER['SERVER_NAME']; ?></h1>
		<div id='secondary'>
			<br />
			<h2>Logout Successful!</h2>
			<hr />
			<h3>Redirecting back to the home page.</h3>
			<br />
		</div>
	</div>
</body>
</html>
<?php		exit( 0 );
		}
	}

if ( ! $logging_out ) {
//	If ya was already informed you failed too many login attempts, GO AWAY!
	if ( @$my_session['visit-result'] == 'Denied!' ) {
		header( 'Location: http://' . gethostbyaddr( $_SERVER['REMOTE_ADDR'] ) . '/' );
		die();
		}

//	Too many log-in failures? - No more tries for the day!
	if ( isset( $my_session['login-attempts'] ) && $my_session['login-attempts'] <= 0 ) {
		$my_session['visit-result'] = 'Denied!';
		$my_session['session-hash'] = LoPro_check_cookie( $my_session );

		LoPro_put2( "$cookie_path.{$my_session['id']}", LoPro_cookie_encode( $my_session ) );

		header( 'HTTP/1.1 503 Server Busy', true, 503 );
		die( "<h2>Try again, some other day.</h2><hr /><p>Bye now!</p>" );
		}
	}

//	Set up custom Session-Cookie data for new session. - I could probably shorten this? ? ?
if ( ! @$my_session['this-session'] ) {
	$my_session = [
		'id'				=> LoPro_gen_session_name(),
		'this-session'		=> "wp-li-" . base64_encode( md5( $_SERVER['REMOTE_ADDR'], true ) ) . "-" . base64_encode( sha1( date( 'Y-m-d/H:i:s' ) . microtime() . $_SERVER['UNIQUE_ID'], true ) ),
		'visiting-time'		=> date( 'Y-m-d/H:i:s' ),
		'visiting-from'		=> '',
		'visitor-ip'		=> $_SERVER['REMOTE_ADDR'],
		'visitor-url'		=> gethostbyaddr( $_SERVER['REMOTE_ADDR'] ),
		'visitor-ua'		=> @htmlentities( $_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8' ),
		'login-attempts'	=> 3,	//	This is how many tries you get ( unless, you are very "forgetful" ).
		'visit-result'		=> 'login',
		'session-hash'		=> ''
		 ];
	}

$my_session['visiting-from'] = @urlencode( $_SERVER['HTTP_REFERER']  );

//	Stop those attempting to use "bad log-in names" to probe my site!
if ( @in_array( $_POST['log'], $bad_logins ) ) {

//	Deliberately set "bad cookie" - so as to preclude this session from any further log-in attempts.
	$my_session['visit-result'] = $my_session['session-hash'] = 'Bad Cookie!';
	LoPro_put2( "$cookie_path.{$my_session['id']}", LoPro_cookie_encode( $my_session ) );
	LoPro_drop_dead_mate();
	}

unset( $bad_logins );

//	Require a valid email address for User-Login input.
if ( USE_EMAIL_NAME && ! @empty( $_POST['log'] ) ) {
	if ( ! preg_match( '#([a-z0-9_\.]{3,})@([a-z0-9_-]{3,})((\.[a-z0-9]{2,7})+)#i', $_POST['log'] ) ) $_POST['log'] = 'Bad Log-In Name!';

//	Test if given host name even supports MX records.
//	elseif ( ! checkdnsrr( trim( strstr( $_POST['log'], '@', false ), '@' ), 'MX' ) ) $_POST['log'] = 'Bad Log-In Name!';
	}

//	This is for those who try to "probe" my login module: Adds an extra "demerit"!
if ( @$_GET['action'] == 'lostpassword' ) $my_session['login-attempts']-=2;
elseif ( @$_POST['wp-submit'] == 'Log In' ) $my_session['login-attempts']--;

//	Built-in cookie-checksum: To prevent "forged" or otherwise altered session-cookies!
$my_session['session-hash'] = LoPro_check_cookie( $my_session );
LoPro_put2( "$cookie_path.{$my_session['id']}", LoPro_cookie_encode( $my_session ) );

// If set, set the "loitering time" on the login-in screen by sending its response header field.
if ( defined( 'HANGOUT' ) && (int)HANGOUT ) header( 'Refresh: ' . HANGOUT . '; url=' . dirname( $_SERVER['PHP_SELF'] ) );

//	Now, on to the log-in module ( or custom log-in module in this case ).
require( __DIR__ . '/' . LOGIN_MODULE );

