# WP-Login-Protector

# Version: 1.03.02
# Release date: 2022-03-20

A small, drop-in module to use with your WP-based site which does not require any coding changes or updates to your WP-installation.

Just upload to your main "public" folder, and add a one-line .htaccess directive - pointing to it.

WHAT THIS MODULE DOES:

1. With the proper .htaccess redirect set up, any requests for "wp-login.php" - will be silently redirected to "Login-Protect.php" - without any evidence to the visitor.

2. WP-Login-Protector will create a "temporary cache" folder of its own - to store its server-side "cookies". - In other words, this happens, regardless if the visitor's User-Agent has JavaScripts disabled - because the "cookies" are handled and usedly completely on the server end - NOT the client's end.

3. WP-Login-Protector makes sure that ANY login attempt MUST use an email address as the "login name", or it will simply refuse to allow the login from happening, again - completely without the visitor's intervention or knowledge.

4. WP-Login-Protector will also keep track of failed log-in attempts, by use of its "secret cookie", and after so many failed attempts - disallow any further attempts for at least one day.

5. WP-Login-Protector will also filter out bad log-in names from being used, by simply looking in a file called "bad_logins.txt.cfg" - and failing any log-in attempts using one of those banned names. If there appears to be a heavy usage of certain log-in names in attempted hacks, all you need to do - as add those additional "suspect name" into that file.

6. WP-Login-Protector will also block attempts to access the log-in screen if the "default WP-Test-Cookie" is set - if the constant within Login-Protector.php is set to -true-. (This also needs to be followed up with the appropriate setting of the constant 'TEST_COOKIE' in the wp-config.php file to something other than 'wordpress-test-cookie'.)

7. WP-Login-Protector will also set a 'HTTP-Refresh' header to the chosen, configured value (in seconds) - which sets the maximum 'loitering time' on the WP-Login page. In other words, after the 'loitering time' has elapsed - if the log-in screen is still visible, it will be automatically redirected back to the home page.

8. WP-Login-Protector is also able to detect any attempts at cookie-tampering, and invalid them if found to have been tampered with. - This effectively terminates any further attempts to use the log-in form.

This is a "drop-in" solution that does not require installation of any other plug-ins in order to work.

It will remain in place, and continue guarding your login module even after major updates of your software.

AND, it helps to further secure your WP-based site from further at hacking attempts - IE: via abusing the login module.

With minor changes to the coding (namely on the "logout page), you can style the logout page to your liking!

A nice, compact solution to help improve at least one more option at improved security.

( Even proofs against abusing the request headers by attempts at injecting dangerous meta-characters and scripting cmds! )

- Please read the comments at the defined constants - to learn what they do.

The file: 'bad_logins.txt.cfg' - should be placed just above your server 'Document-Root' for added security.

